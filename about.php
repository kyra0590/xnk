﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="about-us-page">
   <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
        <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>
                            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a> <span>/</span></li>
                            <li> <strong>About Us</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- main-container -->
        <div class="main-container col2-right-layout">
            <div class="container">
                <div class="row">
                    <section class="col-sm-9">
                        <div class="col-main">
                            <div class="static-inner">
                                <div class="page-title">
                                    <h2>About Us</h2>
                                </div>
                                <div class="static-contain">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                                    <br>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem IpsumLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                                    <br>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <aside class="col-right sidebar col-sm-3 col-xs-12">
                        <div class="block block-company">
                            <div class="block-title">Company </div>
                            <div class="block-content">
                                <ol id="recently-viewed-items">
                                    <li class="item"><strong><i class="fa fa-angle-right"></i> About Us</strong></li>
                                    <li class="item"><a href="sitemap.html"><i class="fa fa-angle-right"></i> Sitemap</a></li>
                                    <li class="item"><a href="#"><i class="fa fa-angle-right"></i> Terms of Service</a></li>
                                    <li class="item"><a href="#"><i class="fa fa-angle-right"></i> Search Terms</a></li>
                                    <li class="item"><a href="contact.html"><i class="fa fa-angle-right"></i> Contact Us</a></li>
                                </ol>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <!--End main-container -->
        <!-- Footer -->
       <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/revslider.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
</body>

</html>