<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="cms-index-index cms-home-page">
    <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
        <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- Revslider -->
        <div class="container jtv-home-revslider">
            <div class="row">
                <div class="col-lg-9 col-sm-9 col-xs-12 jtv-main-home-slider">
                    <div id='rev_slider_1_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
                        <div id='rev_slider_1' class='rev_slider fullwidthabanner'>
                            <ul>
                                <li data-transition='slotzoom-horizontal' data-slotamount='7' data-masterspeed='1000' data-thumb='images/slider/slide-img1.jpg'><img src='images/slider/slide-img1.jpg' alt="slider image1" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                                    <div class="info">
                                        <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-x='0' data-y='165' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;white-space:nowrap;'><span>Shop The Trend</span></div>
                                        <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='0' data-y='220' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;white-space:nowrap;'>Amazing Chance!</div>
                                        <div class='tp-caption Title sft  tp-resizeme ' data-x='0' data-y='300' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'>Our new arrivals can't wait to meet you.</div>
                                        <div class='tp-caption sfb  tp-resizeme ' data-x='0' data-y='350' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><a href='#' class="buy-btn">Browse Now</a></div>
                                    </div>
                                </li>
                                <li data-transition='slotzoom-horizontal' data-slotamount='7' data-masterspeed='1000' data-thumb='images/slider/slide-img3.jpg'><img src='images/slider/slide-img3.jpg' alt="slider image2" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                                    <div class="info">
                                        <div class='tp-caption ExtraLargeTitle sft slide2  tp-resizeme ' data-x='45' data-y='165' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;white-space:nowrap;padding-right:0px'><span>Spring Fashion</span></div>
                                        <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45' data-y='220' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;white-space:nowrap;'>Be Summer Ready</div>
                                        <div class='tp-caption Title sft  tp-resizeme ' data-x='45' data-y='300' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'>Identify your Look, Define your Style!</div>
                                        <div class='tp-caption sfb  tp-resizeme ' data-x='45' data-y='350' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><a href='#' class="buy-btn">Join us</a></div>
                                    </div>
                                </li>
                                <li data-transition='slotzoom-horizontal' data-slotamount='7' data-masterspeed='1000' data-thumb='images/slider/slide-img2.jpg'><img src='images/slider/slide-img2.jpg' alt="slider image3" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                                    <div class="info">
                                        <div class='tp-caption ExtraLargeTitle sft slide2  tp-resizeme ' data-x='45' data-y='165' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;white-space:nowrap;padding-right:0px'><span>Big Sale</span></div>
                                        <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-x='45' data-y='220' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;white-space:nowrap;'>New Fashion</div>
                                        <div class='tp-caption Title sft  tp-resizeme ' data-x='45' data-y='300' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'>Look great & feel amazing in our stunning dresses.</div>
                                        <div class='tp-caption sfb  tp-resizeme ' data-x='45' data-y='350' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;white-space:nowrap;'><a href='#' class="buy-btn">Buy Now</a></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="banner-block"> <a href="#"> <img src="images/banner1.jpg" alt=""> </a>
                        <div class="text-des-container pad-zero">
                            <div class="text-des">
                                <p>Designer</p>
                                <h2>Handbags</h2>
                            </div>
                        </div>
                    </div>
                    <div class="banner-block"> <a href="#"> <img src="images/banner2.jpg" alt=""> </a>
                        <div class="text-des-container">
                            <div class="text-des">
                                <p>The Ultimate</p>
                                <h2>Shoes Collection</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Support Policy Box -->
        <div class="container">
            <div class="support-policy-box">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="support-policy"> <i class="fa fa-truck"></i>
                            <div class="content">Free Shipping on order over $49</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="support-policy"> <i class="fa fa-phone"></i>
                            <div class="content">Need Help +1 123 456 7890</div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="support-policy"> <i class="fa fa-refresh"></i>
                            <div class="content">30 days return Service</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Container -->
        <section class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <div class="col-main">
                            <div class="jtv-featured-products">
                                <div class="slider-items-products">
                                    <div class="jtv-new-title">
                                        <h2>Featured Products</h2>
                                    </div>
                                    <div id="featured-slider" class="product-flexslider hidden-buttons">
                                        <div class="slider-items slider-width-col4 products-grid">
                                            <div class="item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                            <div class="new-label new-top-left">new</div>
                                                            <div class="mask-shop-white"></div>
                                                            <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                            <div class="item-content">
                                                                <div class="item-price">
                                                                    <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                                    </div>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="add_cart">
                                                                        <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                            <div class="mask-shop-white"></div>
                                                            <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>

                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                            <div class="item-content">
                                                                <div class="item-price">
                                                                    <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="add_cart">
                                                                        <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                            <div class="mask-shop-white"></div>
                                                            <div class="new-label new-top-left">new</div>
                                                            <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>

                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                            <div class="item-content">
                                                                <div class="item-price">
                                                                    <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                                    </div>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="add_cart">
                                                                        <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                            <div class="sale-label sale-top-left">Sale</div>
                                                            <div class="mask-shop-white"></div>
                                                            <div class="new-label new-top-left">new</div>
                                                            <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>

                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                            <div class="item-content">
                                                                <div class="item-price">
                                                                    <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="add_cart">
                                                                        <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                            <div class="mask-shop-white"></div>
                                                            <div class="new-label new-top-left">new</div>
                                                            <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>

                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                            <div class="item-content">
                                                                <div class="item-price">
                                                                    <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="add_cart">
                                                                        <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="item-inner">
                                                    <div class="item-img">
                                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                            <div class="mask-shop-white"></div>
                                                            <div class="new-label new-top-left">new</div>
                                                            <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="item-info">
                                                        <div class="info-inner">
                                                            <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                            <div class="item-content">
                                                                <div class="item-price">
                                                                    <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                                </div>
                                                                <div class="actions">
                                                                    <div class="add_cart">
                                                                        <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Trending Products Slider -->
                        <div class="jtv-trending-products">
                            <div class="slider-items-products">
                                <div class="jtv-new-title">
                                    <h2>Trending Products</h2>
                                </div>
                                <div id="trending-slider" class="product-flexslider hidden-buttons">
                                    <div class="slider-items slider-width-col4 products-grid">
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="new-label new-top-left">new</div>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                                </div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="sale-label sale-top-right">Sale</div>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="sale-label sale-top-right">Sale</div>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                                </div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="sale-label sale-top-left">Sale</div>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">$189.00</span></span></div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="item-inner">
                                                <div class="item-img">
                                                    <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                        <div class="mask-shop-white"></div>
                                                        <div class="new-label new-top-left">new</div>
                                                        <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                    </div>
                                                </div>
                                                <div class="item-info">
                                                    <div class="info-inner">
                                                        <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                        <div class="item-content">
                                                            <div class="item-price">
                                                                <div class="price-box"> <span class="regular-price"> <span class="price">$219.00</span></span></div>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="add_cart">
                                                                    <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Trending Products Slider -->
                        <!-- Latest Blog -->
                        <div class="jtv-latest-blog">
                            <div class="jtv-new-title">
                                <h2>Latest News</h2>
                            </div>
                            <div class="row">
                                <div class="blog-outer-container">
                                    <div class="blog-inner">
                                        <div class="col-xs-12 col-sm-4 blog-preview_item">
                                            <div class="entry-thumb jtv-blog-img-hover"> <a href="blog_single_post.html"> <img alt="Blog" src="images/blog-img1.jpg"> </a> </div>
                                            <h4 class="blog-preview_title"><a href="blog_single_post.html">Neque porro quisquam est qui</a></h4>
                                            <div class="blog-preview_info">
                                                <ul class="post-meta">
                                                    <li><i class="fa fa-user"></i>By <a href="#">admin</a></li>
                                                    <li><i class="fa fa-comments"></i><a href="#">8 comments</a></li>
                                                    <li><i class="fa fa-clock-o"></i><span class="day">12</span><span class="month">Feb</span></li>
                                                </ul>
                                                <div class="blog-preview_desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a class="read_btn" href="blog_single_post.html">Read More</a></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 blog-preview_item">
                                            <div class="entry-thumb jtv-blog-img-hover"> <a href="blog_single_post.html"> <img alt="Blog" src="images/blog-img1.jpg"> </a> </div>
                                            <h4 class="blog-preview_title"><a href="blog_single_post.html">Neque porro quisquam est qui</a></h4>
                                            <div class="blog-preview_info">
                                                <ul class="post-meta">
                                                    <li><i class="fa fa-user"></i>By <a href="#">admin</a></li>
                                                    <li><i class="fa fa-comments"></i><a href="#">20 comments</a></li>
                                                    <li><i class="fa fa-clock-o"></i><span class="day">25</span><span class="month">Feb</span></li>
                                                </ul>
                                                <div class="blog-preview_desc">Lorem Ipsum is simply dummy text of the printing and typesetting industry. <a class="read_btn" href="blog_single_post.html">Read More</a></div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 blog-preview_item">
                                            <div class="entry-thumb jtv-blog-img-hover"> <a href="blog_single_post.html"> <img alt="Blog" src="images/blog-img1.jpg"> </a> </div>
                                            <h4 class="blog-preview_title"><a href="blog_single_post.html">Dolorem ipsum quia dolor sit amet</a></h4>
                                            <div class="blog-preview_info">
                                                <ul class="post-meta">
                                                    <li><i class="fa fa-user"></i>By <a href="#">admin</a></li>
                                                    <li><i class="fa fa-comments"></i><a href="#">8 comments</a></li>
                                                    <li><i class="fa fa-clock-o"></i><span class="day">15</span><span class="month">Jan</span></li>
                                                </ul>
                                                <div class="blog-preview_desc">Sed ut perspiciatis unde omnis iste natus error sit voluptatem dolore lauda. <a class="read_btn" href="blog_single_post.html">Read More</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Latest Blog -->
                    </div>
                </div>
            </div>
        </section>
        <!-- Collection Banner -->
        <div class="jtv-collection-area">
            <div class="container">
                <div class="column-right pull-left col-sm-4 no-padding"> <a href="#"> <img src="images/women-top.jpg" alt="Top Collections"> </a>
                    <div class="col-right-text">
                        <h5 class="text-uppercase">Top Collections <span> 35% </span> get it now</h5>
                    </div>
                </div>
                <div class="column-left pull-right col-sm-8 no-padding">
                    <div class="column-left-top">
                        <div class="col-left-top-left pull-left col-sm-8 no-padding"> <a href="#"> <img src="images/men-suits.jpg" alt="Men's Suits"> </a>
                            <div class="col-left-top-left-text">
                                <h5 class="text-uppercase">Dressing for your Wedding</h5>
                                <h3 class="text-uppercase">Men's Suits</h3>
                                <h5 class="text-uppercase">Look Good, Feel Good</h5>
                            </div>
                        </div>
                        <div class="col-left-top-right pull-right col-sm-4 no-padding"> <a href="#"> <img src="images/footwear.jpg" alt="footwear"> </a>
                            <div class="col-left-top-right-text text-center">
                                <h5 class="text-uppercase">Footwear Fashion Sale</h5>
                                <h3>50%</h3>
                                <h5 class="text-uppercase">Buy 1, Get 1</h5>
                            </div>
                        </div>
                    </div>
                    <div class="column-left-bottom col-sm-12 no-padding">
                        <div class="col-left-bottom-left pull-left col-sm-4 no-padding"> <a href="#"> <img src="images/handbag.jpg" alt="Handbag"> </a>
                            <div class="col-left-bottom-left-text">
                                <h5 class="text-uppercase">What's New?</h5>
                                <h3 class="text-uppercase">Bag's</h3>
                                <h5 class="text-uppercase">Everyday<br>
                                    Low Prices</h5>
                            </div>
                        </div>
                        <div class="col-left-bottom-right pull-right col-sm-8 no-padding"> <a href="#"> <img src="images/watch-banner.jpg" alt="Watch"> </a>
                            <div class="col-left-bottom-right-text">
                                <h5 class="text-uppercase">Never Miss a Second</h5>
                                <h3 class="text-uppercase">Watch</h3>
                                <h5 class="text-uppercase">Time to buy a watch!</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Brand Logo -->
        <div class="container jtv-brand-logo-block">
            <!-- Footer -->
            <?php include('include/footer.php') ?>
        </div>
        <!-- JavaScript -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/revslider.js"></script>
        <script src="js/main.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/mob-menu.js"></script>
        <script src="js/countdown.js"></script>
        <script>
        jQuery(document).ready(function() {
            jQuery('#rev_slider_1').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 858,
                startheight: 500,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',

                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,

                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'off',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'off',
                forceFullWidth: 'on',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,

                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
        </script>
        <!-- Hot Deals Timer -->
        <script>
        var dthen1 = new Date("12/25/17 11:59:00 PM");
        start = "08/04/15 03:02:11 AM";
        start_date = Date.parse(start);
        var dnow1 = new Date(start_date);
        if (CountStepper > 0)
            ddiff = new Date((dnow1) - (dthen1));
        else
            ddiff = new Date((dthen1) - (dnow1));
        gsecs1 = Math.floor(ddiff.valueOf() / 1000);

        var iid1 = "countbox_1";
        CountBack_slider(gsecs1, "countbox_1", 1);
        </script>
</body>

</html>