﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body>
    <!-- Mobile Menu -->
     <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
        <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>
                            <li><a href="index.html" title="Go to Home Page">Home</a><span>/</span></li>
                            <li><a title="women" href="shop-grid-sidebar.html">Women</a><span>/</span></li>
                            <li><strong>Clothing</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->
        <!-- Main Container -->
        <div class="main-container col2-left-layout">
            <div class="container">
                <div class="row">
                    <div class="col-sm-9 col-sm-push-3 main-inner">
                        <div class="category-description std">
                            <div class="slider-items-products">
                                <div id="category-desc-slider" class="product-flexslider hidden-buttons">
                                    <div class="slider-items slider-width-col1 owl-carousel owl-theme">
                                        <div class="item"> <a href="#"><img alt="New Special Collection" src="images/new-special.jpg"></a>
                                            <div class="cat-img-title cat-bg cat-box">
                                                <h2 class="cat-heading">New Special<br>
                                                    Collection</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                                            </div>
                                        </div>
                                        <div class="item"> <a href="#"><img alt="New Fashion" src="images/new-fashion.jpg"></a>
                                            <div class="cat-img-title cat-bg cat-box">
                                                <h2 class="cat-heading">New Fashion</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <article class="col-main">
                            <div class="page-title">
                                <h2>Clothing</h2>
                            </div>
                            <div class="toolbar">
                                <div class="sorter">
                                    <div class="view-mode"> <span title="Grid" class="button button-active button-grid">&nbsp;</span><a href="shop-list-sidebar.html" title="List" class="button-list">&nbsp;</a> </div>
                                </div>
                                <div id="sort-by">
                                    <label class="left">Sort By: </label>
                                    <ul>
                                        <li><a href="#">Position<span class="right-arrow"></span></a>
                                            <ul>
                                                <li><a href="#">Name</a></li>
                                                <li><a href="#">Price</a></li>
                                                <li><a href="#">Position</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <a class="button-asc left" href="#" title="Set Descending Direction"><span class="top_arrow"></span></a>
                                </div>
                                <div class="pager">
                                    <div id="limiter">
                                        <label>View: </label>
                                        <ul>
                                            <li><a href="#">15<span class="right-arrow"></span></a>
                                                <ul>
                                                    <li><a href="#">20</a></li>
                                                    <li><a href="#">30</a></li>
                                                    <li><a href="#">35</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="category-products">
                                <ul class="products-grid">
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="new-label new-top-left">new</div>
                                                    <div class="sale-label sale-top-right">sale</div>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                            </div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                            </div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="sale-label sale-top-left">sale</div>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="new-label new-top-left">new</div>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                            </div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span>
                                                            </div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="new-label new-top-left">new</div>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="new-label new-top-left">new</div>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="new-label new-top-left">new</div>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                        <div class="item-inner">
                                            <div class="item-img">
                                                <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="product-detail.html"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                                    <div class="mask-shop-white"></div>
                                                    <div class="new-label new-top-left">new</div>
                                                     <a class="quickview-btn" href="product-detail-sidebar.php"><span>Quick View</span></a>
                                                </div>
                                            </div>
                                            <div class="item-info">
                                                <div class="info-inner">
                                                    <div class="item-title"> <a title="Product tilte is here" href="product-detail.html">Product tilte is here </a> </div>
                                                    <div class="item-content">
                                                        <div class="item-price">
                                                            <div class="price-box"> <span class="regular-price"> <span class="price">Liên hệ</span></span></div>
                                                        </div>
                                                        <div class="actions">
                                                            <div class="add_cart">
                                                                <button class="button btn-cart" type="button"><span><i class="fa fa-shopping-cart"></i> Add to Cart</span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="toolbar bottom">
                                <div class="row">
                                    <div class="col-sm-6 text-left">
                                        <div class="pages">
                                            <ul class="pagination">
                                                <li><a href="#">«</a></li>
                                                <li class="active"><a href="#">1</a></li>
                                                <li><a href="#">2</a></li>
                                                <li><a href="#">3</a></li>
                                                <li><a href="#">»</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 text-right">Showing 1 to 15 of 25 (2 Pages)</div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="sidebar col-sm-3 col-xs-12 col-sm-pull-9">
                        <aside class="sidebar">
                            <div class="block block-layered-nav">
                                <div class="block-title">
                                    <h3>Shop By</h3>
                                </div>
                                <div class="block-content">
                                    <p class="block-subtitle">Shopping Options</p>
                                    <dl id="narrow-by-list">
                                        <dt class="even">Manufacturer</dt>
                                        <dd class="even">
                                            <ol>
                                                <li><a href="#">Kids Dresses</a> (20) </li>
                                                <li><a href="#">Unisex Clothing</a> (25) </li>
                                                <li><a href="#">Winter Wear</a> (8) </li>
                                                <li><a href="#">Garments</a> (5) </li>
                                                <li><a href="#">Undergarments</a> (2) </li>
                                            </ol>
                                        </dd>
                                        <dt class="odd">Clothing Material</dt>
                                        <dd class="odd">
                                            <ol class="bag-material">
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Cotton" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Cotton</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Denim" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Denim</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Linen" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Linen</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Rayon" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Rayon</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Synthetic" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Synthetic</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Satin" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Satin</label>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="pretty p-icon p-smooth">
                                                        <input type="checkbox" name="Material" value="Silk" />
                                                        <div class="state p-success"> <i class="icon fa fa-check"></i>
                                                            <label>Silk</label>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </dd>
                                        <dt class="odd">Size</dt>
                                        <div class="size-area">
                                            <div class="size">
                                                <ul>
                                                    <li><a href="#">S</a></li>
                                                    <li><a href="#">L</a></li>
                                                    <li><a href="#">M</a></li>
                                                    <li><a href="#">XL</a></li>
                                                    <li><a href="#">XXL</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <dt class="odd">Color</dt>
                                        <dd class="odd">
                                            <ol>
                                                <li><a href="#">Green</a> (1) </li>
                                                <li><a href="#">White</a> (5) </li>
                                                <li><a href="#">Black</a> (5) </li>
                                                <li><a href="#">Gray</a> (4) </li>
                                                <li><a href="#">Dark Gray</a> (3) </li>
                                                <li><a href="#">Blue</a> (1) </li>
                                            </ol>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="custom-slider">
                                <div>
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <ol class="carousel-indicators">
                                            <li class="active" data-target="#carousel-example-generic" data-slide-to="0"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                        </ol>
                                        <div class="carousel-inner">
                                            <div class="item active"><img src="images/slide3.jpg" alt="New Arrivals">
                                                <div class="carousel-caption">
                                                    <h3><a title=" Sample Product" href="#">New Arrivals</a></h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                </div>
                                            </div>
                                            <div class="item"><img src="images/slide1.jpg" alt="Top Fashion">
                                                <div class="carousel-caption">
                                                    <h3><a title=" Sample Product" href="#">Top Fashion</a></h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                </div>
                                            </div>
                                            <div class="item"><img src="images/slide2.jpg" alt="Mid Season">
                                                <div class="carousel-caption">
                                                    <h3><a title=" Sample Product" href="#">Mid Season</a></h3>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <a class="left carousel-control" href="#" data-slide="prev"> <span class="sr-only">Previous</span></a> <a class="right carousel-control" href="#" data-slide="next"> <span class="sr-only">Next</span></a>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Container End -->
        <!-- Footer -->
         <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
</body>

</html>