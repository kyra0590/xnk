﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="checkout-page">
    <!-- Mobile Menu -->
   <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
       <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>
                            <li class="home"> <a href="index.html" title="Go to Home Page">Home</a> <span>/</span></li>
                            <li> <strong>Checkout</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->
        <!-- main-container -->
        <div class="main-container col1-layout">
            <div class="main container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="product-area">
                            <div class="title-tab-product-category">
                                <div class="text-center">
                                    <ul class="nav jtv-heading-style" role="tablist">
                                        <li role="presentation" class=""><a href="#cart" aria-controls="cart" role="tab" data-toggle="tab">1 Shopping cart</a></li>
                                        <li role="presentation" class=""><a href="#checkout" aria-controls="checkout" role="tab" data-toggle="tab">2 Checkout</a></li>
                                        <li role="presentation" class="active"><a href="#complete-order" aria-controls="complete-order" role="tab" data-toggle="tab">3 Complete Order</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="content-tab-product-category">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in" id="cart">
                                        <!-- cart are start-->
                                        <div class="cart-page-area">
                                            <form method="post" action="#">
                                                <div class="table-responsive">
                                                    <table class="shop-cart-table">
                                                        <thead>
                                                            <tr>
                                                                <th class="product-thumbnail ">Image</th>
                                                                <th class="product-name ">Product Name</th>
                                                                <th class="product-quantity">Quantity</th>
                                                                <th class="product-remove">Remove</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr class="cart_item">
                                                                <td class="item-img"><a href="#"><img src="images/products/product-fashion-1.jpg" alt="Product tilte is here "> </a></td>
                                                                <td class="item-title"><a href="#">Product tilte is here </a></td>
                                                                <td class="item-qty">
                                                                    <div class="cart-quantity">
                                                                        <div class="product-qty">
                                                                            <div class="cart-quantity">
                                                                                <div class="cart-plus-minus">
                                                                                    <div class="dec qtybutton">-</div>
                                                                                    <input value="2" name="qtybutton" class="cart-plus-minus-box" type="text">
                                                                                    <div class="inc qtybutton">+</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td class="remove-item"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>
                                                            <tr class="cart_item">
                                                                <td class="item-img"><a href="#"><img src="images/products/product-fashion-1.jpg" alt="Product tilte is here "> </a></td>
                                                                <td class="item-title"><a href="#">Product tilte is here </a></td>
                                                                <td class="item-qty">
                                                                    <div class="cart-quantity">
                                                                        <div class="product-qty">
                                                                            <div class="cart-quantity">
                                                                                <div class="cart-plus-minus">
                                                                                    <div class="dec qtybutton">-</div>
                                                                                    <input value="1" name="qtybutton" class="cart-plus-minus-box" type="text">
                                                                                    <div class="inc qtybutton">+</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td class="remove-item"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>
                                                            <tr class="cart_item">
                                                                <td class="item-img"><a href="#"><img src="images/products/product-fashion-1.jpg" alt="Product tilte is here "> </a></td>
                                                                <td class="item-title"><a href="#">Product tilte is here </a></td>
                                                                <td class="item-qty">
                                                                    <div class="cart-quantity">
                                                                        <div class="product-qty">
                                                                            <div class="cart-quantity">
                                                                                <div class="cart-plus-minus">
                                                                                    <div class="dec qtybutton">-</div>
                                                                                    <input value="1" name="qtybutton" class="cart-plus-minus-box" type="text">
                                                                                    <div class="inc qtybutton">+</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td class="remove-item"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>
                                                            <tr class="cart_item">
                                                                <td class="item-img"><a href="#"><img src="images/products/product-fashion-1.jpg" alt="Product tilte is here "> </a></td>
                                                                <td class="item-title"><a href="#">Product tilte is here </a></td>
                                                                <td class="item-qty">
                                                                    <div class="cart-quantity">
                                                                        <div class="product-qty">
                                                                            <div class="cart-quantity">
                                                                                <div class="cart-plus-minus">
                                                                                    <div class="dec qtybutton">-</div>
                                                                                    <input value="1" name="qtybutton" class="cart-plus-minus-box" type="text">
                                                                                    <div class="inc qtybutton">+</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td class="remove-item"><a href="#"><i class="fa fa-trash-o"></i></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="cart-bottom-area">
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="update-coupne-area">
                                                                <div class="update-continue-btn text-right">
                                                                    <button class="button btn-continue" title="Continue Shopping" type="button"><span>Continue Shopping</span></button>
                                                                    <button class="button btn-empty" title="Clear Cart" value="empty_cart" name="update_cart_action" type="submit"><span>Clear Cart</span></button>
                                                                    <button class="button btn-update" title="Update Cart" value="update_qty" name="update_cart_action" type="submit"><span>Update Cart</span></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- cart are end-->
                                    </div>
                                    <div role="tabpanel" class="tab-pane  fade in" id="checkout">
                                        <!-- Checkout are start-->
                                        <div class="checkout-area">
                                            <div class="">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-md-6 col-xs-12">
                                                                <div class="billing-details">
                                                                    <div class="contact-text right-side">
                                                                        <h2>Billing Details</h2>
                                                                        <form action="#">
                                                                            <div class="row">
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>First Name <em>*</em></label>
                                                                                        <input type="text" name="namm" class="info" placeholder="First Name">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Last Name<em>*</em></label>
                                                                                        <input type="text" name="namm" class="info" placeholder="Last Name">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Company Name</label>
                                                                                        <input type="text" name="cpany" class="info" placeholder="Company Name">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Email Address<em>*</em></label>
                                                                                        <input type="email" name="email" class="info" placeholder="Your Email">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Phone Number<em>*</em></label>
                                                                                        <input type="text" name="phone" class="info" placeholder="Phone Number">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Country <em>*</em></label>
                                                                                        <select class="selectpicker select-custom" data-live-search="true">
                                                                                            <option data-tokens="Vietnam">Vietnam</option>
                                                                                            <option data-tokens="India">India</option>
                                                                                            <option data-tokens="Pakistan">Pakistan</option>
                                                                                            <option data-tokens="Pakistan">Pakistan</option>
                                                                                            <option data-tokens="Srilanka">Srilanka</option>
                                                                                            <option data-tokens="Nepal">Nepal</option>
                                                                                            <option data-tokens="Butan">Butan</option>
                                                                                            <option data-tokens="USA">USA</option>
                                                                                            <option data-tokens="England">England</option>
                                                                                            <option data-tokens="Brazil">Brazil</option>
                                                                                            <option data-tokens="Canada">Canada</option>
                                                                                            <option data-tokens="China">China</option>
                                                                                            <option data-tokens="Koeria">Koeria</option>
                                                                                            <option data-tokens="Soudi">Soudi Arabia</option>
                                                                                            <option data-tokens="Spain">Spain</option>
                                                                                            <option data-tokens="France">France</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Address <em>*</em></label>
                                                                                        <input type="text" name="add1" class="info mb-10" placeholder="Street Address">
                                                                                        <input type="text" name="add2" class="info mt10" placeholder="Apartment, suite, unit etc. (optional)">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Town/City <em>*</em></label>
                                                                                        <input type="text" name="add1" class="info" placeholder="Town/City">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>State/Divison <em>*</em></label>
                                                                                        <select class="selectpicker select-custom" data-live-search="true">
                                                                                            <option data-tokens="Barisal">Barisal</option>
                                                                                            <option data-tokens="Dhaka">Dhaka</option>
                                                                                            <option data-tokens="Kulna">Kulna</option>
                                                                                            <option data-tokens="Rajshahi">Rajshahi</option>
                                                                                            <option data-tokens="Sylet">Sylet</option>
                                                                                            <option data-tokens="Chittagong">Chittagong</option>
                                                                                            <option data-tokens="Rangpur">Rangpur</option>
                                                                                            <option data-tokens="Maymanshing">Maymanshing</option>
                                                                                            <option data-tokens="Cox">Cox's Bazar</option>
                                                                                            <option data-tokens="Saint">Saint Martin</option>
                                                                                            <option data-tokens="Kuakata">Kuakata</option>
                                                                                            <option data-tokens="Sajeq">Sajeq</option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                    <div class="input-box">
                                                                                        <label>Post Code/Zip Code<em>*</em></label>
                                                                                        <input type="text" name="zipcode" class="info" placeholder="Zip Code">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="create-acc clearfix">
                                                                                        <div class="acc-toggle">
                                                                                            <input type="checkbox" id="acc-toggle">
                                                                                            <label for="acc-toggle">Create an Account ?</label>
                                                                                        </div>
                                                                                        <div class="create-acc-body">
                                                                                            <div class="sm-des"> Create an account by entering the information below. If you are a returning customer please login at the top of the page. </div>
                                                                                            <div class="input-box">
                                                                                                <label>Account password <em>*</em></label>
                                                                                                <input type="password" name="pass" class="info" placeholder="Password">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12">
                                                                <div class="billing-details">
                                                                    <div class="right-side">
                                                                        <div class="ship-acc clearfix">
                                                                            <div class="ship-toggle">
                                                                                <input type="checkbox" id="ship-toggle">
                                                                                <label for="ship-toggle">Ship to a different address?</label>
                                                                            </div>
                                                                            <div class="ship-acc-body">
                                                                                <form action="#">
                                                                                    <div class="row">
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>First Name <em>*</em></label>
                                                                                                <input type="text" name="namm" class="info" placeholder="First Name">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Last Name<em>*</em></label>
                                                                                                <input type="text" name="namm" class="info" placeholder="Last Name">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Company Name</label>
                                                                                                <input type="text" name="cpany" class="info" placeholder="Company Name">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Email Address<em>*</em></label>
                                                                                                <input type="email" name="email" class="info" placeholder="Your Email">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Phone Number<em>*</em></label>
                                                                                                <input type="text" name="phone" class="info" placeholder="Phone Number">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Country <em>*</em></label>
                                                                                                <select class="selectpicker select-custom" data-live-search="true">
                                                                                                    <option data-tokens="Bangladesh">Bangladesh</option>
                                                                                                    <option data-tokens="India">India</option>
                                                                                                    <option data-tokens="Pakistan">Pakistan</option>
                                                                                                    <option data-tokens="Pakistan">Pakistan</option>
                                                                                                    <option data-tokens="Srilanka">Srilanka</option>
                                                                                                    <option data-tokens="Nepal">Nepal</option>
                                                                                                    <option data-tokens="Butan">Butan</option>
                                                                                                    <option data-tokens="USA">USA</option>
                                                                                                    <option data-tokens="England">England</option>
                                                                                                    <option data-tokens="Brazil">Brazil</option>
                                                                                                    <option data-tokens="Canada">Canada</option>
                                                                                                    <option data-tokens="China">China</option>
                                                                                                    <option data-tokens="Koeria">Koeria</option>
                                                                                                    <option data-tokens="Soudi">Soudi Arabia</option>
                                                                                                    <option data-tokens="Spain">Spain</option>
                                                                                                    <option data-tokens="France">France</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Address <em>*</em></label>
                                                                                                <input type="text" name="add1" class="info mb-10" placeholder="Street Address">
                                                                                                <input type="text" name="add2" class="info mt10" placeholder="Apartment, suite, unit etc. (optional)">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Town/City <em>*</em></label>
                                                                                                <input type="text" name="add1" class="info" placeholder="Town/City">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>State/Divison <em>*</em></label>
                                                                                                <select class="selectpicker select-custom" data-live-search="true">
                                                                                                    <option data-tokens="Barisal">Barisal</option>
                                                                                                    <option data-tokens="Dhaka">Dhaka</option>
                                                                                                    <option data-tokens="Kulna">Kulna</option>
                                                                                                    <option data-tokens="Rajshahi">Rajshahi</option>
                                                                                                    <option data-tokens="Sylet">Sylet</option>
                                                                                                    <option data-tokens="Chittagong">Chittagong</option>
                                                                                                    <option data-tokens="Rangpur">Rangpur</option>
                                                                                                    <option data-tokens="Maymanshing">Maymanshing</option>
                                                                                                    <option data-tokens="Cox">Cox's Bazar</option>
                                                                                                    <option data-tokens="Saint">Saint Martin</option>
                                                                                                    <option data-tokens="Kuakata">Kuakata</option>
                                                                                                    <option data-tokens="Sajeq">Sajeq</option>
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <div class="input-box">
                                                                                                <label>Post Code/Zip Code<em>*</em></label>
                                                                                                <input type="text" name="zipcode" class="info" placeholder="Zip Code">
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form">
                                                                            <div class="input-box">
                                                                                <label>Order Notes</label>
                                                                                <textarea placeholder="Notes about your order, e.g. special notes for delivery." class="area-tex"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Checkout are end-->
                                    </div>
                                    <div role="tabpanel" class="tab-pane  fade in active" id="complete-order">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="checkout-payment-area">
                                                    <div class="checkout-total">
                                                        <h3>Your order</h3>
                                                        <form action="#" method="post">
                                                            <div class="table-responsive">
                                                                <table class="checkout-area table">
                                                                    <thead>
                                                                        <tr class="cart_item check-heading">
                                                                            <td class="ctg-type"> Product</td>
                                                                            <td class="cgt-des"> Quantity</td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr class="cart_item check-item prd-name">
                                                                            <td class="ctg-type"> Product tilte is here × <span>1</span></td>
                                                                            <td class="cgt-des"> 1</td>
                                                                        </tr>
                                                                        <tr class="cart_item check-item prd-name">
                                                                            <td class="ctg-type"> Product tilte is here × <span>1</span></td>
                                                                            <td class="cgt-des"> 1</td>
                                                                        </tr>
                                                                        <tr class="cart_item">
                                                                            <td class="ctg-type"> Subtotal</td>
                                                                            <td class="cgt-des">1</td>
                                                                        </tr>
                                                                        <tr class="cart_item">
                                                                            <td class="ctg-type crt-total"> Total</td>
                                                                            <td class="cgt-des prc-total">3 </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="payment-section">
                                                        <div class="pay-toggle">
                                                            <form action="#">
                                                                <div class="input-box"> <a class="btn-def btn2" href="#">Finish</a> </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="jtv-crosssel-pro">
                    <div class="jtv-new-title">
                        <h2>you may be interested</h2>
                    </div>
                    <div class="category-products">
                        <ul class="products-grid">
                            <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                <div class="item-inner">
                                    <div class="item-img">
                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                            <div class="new-label new-top-left">new</div>
                                            <div class="sale-label sale-top-right">sale</div>
                                            <div class="mask-shop-white"></div>
                                            <div class="new-label new-top-left">new</div>
                                            <a class="quickview-btn" href="quick-view.html"><span>Quick View</span></a> <a href="wishlist.html">
                                                <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                                            </a> <a href="compare.html">
                                                <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="info-inner">
                                            <div class="item-title"> <a title="Product tilte is here" href="#">Product tilte is here </a> </div>
                                            <div class="item-content">
                                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                <div class="item-price">
                                                    <div class="price-box"> <span class="regular-price"> <span class="price">$75.00</span></span></div>
                                                </div>
                                                <div class="actions"><a href="#" class="link-wishlist" title="Add to Wishlist"></a>
                                                    <div class="add_cart">
                                                        <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                                    </div>
                                                    <a href="#" class="link-compare" title="Add to Compare"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                <div class="item-inner">
                                    <div class="item-img">
                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                            <div class="mask-shop-white"></div>
                                            <div class="new-label new-top-left">new</div>
                                            <a class="quickview-btn" href="quick-view.html"><span>Quick View</span></a> <a href="wishlist.html">
                                                <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                                            </a> <a href="compare.html">
                                                <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="info-inner">
                                            <div class="item-title"> <a title="Product tilte is here" href="#">Product tilte is here </a> </div>
                                            <div class="item-content">
                                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                <div class="item-price">
                                                    <div class="price-box"> <span class="regular-price"> <span class="price">$88.99</span></span></div>
                                                </div>
                                                <div class="actions"><a href="#" class="link-wishlist" title="Add to Wishlist"></a>
                                                    <div class="add_cart">
                                                        <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                                    </div>
                                                    <a href="#" class="link-compare" title="Add to Compare"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                <div class="item-inner">
                                    <div class="item-img">
                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                            <div class="mask-shop-white"></div>
                                            <div class="new-label new-top-left">new</div>
                                            <a class="quickview-btn" href="quick-view.html"><span>Quick View</span></a> <a href="wishlist.html">
                                                <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                                            </a> <a href="compare.html">
                                                <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="info-inner">
                                            <div class="item-title"> <a title="Product tilte is here" href="#">Product tilte is here </a> </div>
                                            <div class="item-content">
                                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                <div class="item-price">
                                                    <div class="price-box"> <span class="regular-price"> <span class="price">$149.00</span></span></div>
                                                </div>
                                                <div class="actions"><a href="#" class="link-wishlist" title="Add to Wishlist"></a>
                                                    <div class="add_cart">
                                                        <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                                    </div>
                                                    <a href="#" class="link-compare" title="Add to Compare"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">
                                <div class="item-inner">
                                    <div class="item-img">
                                        <div class="item-img-info"><a class="product-image" title="Product tilte is here" href="#"> <img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"> </a>
                                            <div class="sale-label sale-top-left">sale</div>
                                            <div class="mask-shop-white"></div>
                                            <div class="new-label new-top-left">new</div>
                                            <a class="quickview-btn" href="quick-view.html"><span>Quick View</span></a> <a href="wishlist.html">
                                                <div class="mask-left-shop"><i class="fa fa-heart"></i></div>
                                            </a> <a href="compare.html">
                                                <div class="mask-right-shop"><i class="fa fa-signal"></i></div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-info">
                                        <div class="info-inner">
                                            <div class="item-title"> <a title="Product tilte is here" href="#">Product tilte is here </a> </div>
                                            <div class="item-content">
                                                <div class="rating"> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> </div>
                                                <div class="item-price">
                                                    <div class="price-box"> <span class="regular-price"> <span class="price">$139.55</span></span></div>
                                                </div>
                                                <div class="actions"><a href="#" class="link-wishlist" title="Add to Wishlist"></a>
                                                    <div class="add_cart">
                                                        <button class="button btn-cart" type="button"><span>Add to Cart</span></button>
                                                    </div>
                                                    <a href="#" class="link-compare" title="Add to Compare"></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--End main-container -->
        <!-- Footer -->
        <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/revslider.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
</body>

</html>