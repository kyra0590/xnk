<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="login-page">
    <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- Navigation -->
        <section class="main-container col1-layout">
            <div class="main container">
                <div class="account-login col-md-3">
                    <div class="page-title">
                        <h2>Login Account</h2>
                    </div>
                    <fieldset>
                        <div class="registered-users"><strong>Registered Customers</strong>
                            <div class="content">
                                <p>If you have an account with us, please log in.</p>
                                <ul class="form-list">
                                    <li>
                                        <label for="email">Email Address <span class="required">*</span></label>
                                        <input type="text" title="Email Address" class="input-text required-entry" id="email" value="" name="login[username]">
                                    </li>
                                    <li>
                                        <label for="pass">Password <span class="required">*</span></label>
                                        <input type="password" title="Password" id="pass" class="input-text required-entry validate-password" name="login[password]">
                                    </li>
                                </ul>
                                <p class="required">* Required Fields</p>
                                <div class="buttons-set">
                                    <button id="send2" name="send" type="submit" class="button login"><span>Login</span></button>
                                    <a class="forgot-word" href="forgot-password.html">Forgot Your Password?</a> </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <br>
                <br>
                <br>
                <br>
                <br>
            </div>
        </section>
        <!-- Footer -->
        <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/revslider.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
</body>

</html>