<header>
    <div class="header-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-xs-12">
                    <div class="logo"><a title="ecommerce Template" href="index.php"><img alt="ecommerce Template" src="images/logo.png"></a></div>
                    <div class="nav-icon">
                        <div class="mega-container visible-lg visible-md visible-sm">
                            <div class="navleft-container">
                                <div class="mega-menu-title">
                                    <h3><i class="fa fa-navicon"></i>Categories</h3>
                                </div>
                                <div class="mega-menu-category">
                                    <ul class="nav">
                                        <li><a href="#">Agriculture / Food & Beverage</a>
                                            <div class="wrap-popup">
                                                <div class="popup">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Agriculture</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Shop Grid</span></a></li>
                                                                <li><a href="#"><span>Shop Grid Sidebar</span></a></li>
                                                                <li><a href="#"><span>Shop List</span></a></li>
                                                                <li><a href="#"><span>Shop List Sidebar</span></a></li>
                                                                <li><a href="#"><span>Product Detail</span></a></li>
                                                                <li><a href="#"><span>Product Detail Sidebar</span></a></li>
                                                                <li><a href=""><span>Shopping Cart</span></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Food & Beverage</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Checkout</span></a></li>
                                                                <li><a href="#">Complete Order</a></li>
                                                                <li><a href="#">Account Information</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li><a href="#">Beautty / Personal Care / Headth & Herbal</a>
                                            <div class="wrap-popup">
                                                <div class="popup">
                                                    <div class="row">
                                                        <div class="col-md-4 col-sm-6">
                                                            <h3><a href="#">Beauty</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Shop Grid</span></a></li>
                                                                <li><a href="#"><span>Shop Grid Sidebar</span></a></li>
                                                                <li><a href="#"><span>Shop List</span></a></li>
                                                                <li><a href="#"><span>Shop List Sidebar</span></a></li>
                                                                <li><a href="#"><span>Product Detail</span></a></li>
                                                                <li><a href="#"><span>Product Detail Sidebar</span></a></li>
                                                                <li><a href=""><span>Shopping Cart</span></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4 col-sm-6">
                                                            <h3><a href="#">Pesonal Care</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Checkout</span></a></li>
                                                                <li><a href="#"><span>Quick View</span></a></li>
                                                                <li><a href="#">Complete Order</a></li>
                                                                <li><a href="#">Account Information</a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-4 col-sm-6">
                                                            <h3><a href="#">Heath & Herbal</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Checkout</span></a></li>
                                                                <li><a href="#"><span>Quick View</span></a></li>
                                                                <li><a href="#">Complete Order</a></li>
                                                                <li><a href="#">Account Information</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li><a href="#">Bag / Shoes</a>
                                            <div class="wrap-popup">
                                                <div class="popup">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Bag</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Shop Grid</span></a></li>
                                                                <li><a href="#"><span>Shop Grid Sidebar</span></a></li>
                                                                <li><a href="#"><span>Shop List</span></a></li>
                                                                <li><a href="#"><span>Shop List Sidebar</span></a></li>
                                                                <li><a href="#"><span>Product Detail</span></a></li>
                                                                <li><a href="#"><span>Product Detail Sidebar</span></a></li>
                                                                <li><a href=""><span>Shopping Cart</span></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Shoes</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Checkout</span></a></li>
                                                                <li><a href="#"><span>Quick View</span></a></li>
                                                                <li><a href="#">Complete Order</a></li>
                                                                <li><a href="#">Account Information</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li><a href="#">Home Garden / Contruction</a>
                                            <div class="wrap-popup">
                                                <div class="popup">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Home Garden</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Shop Grid</span></a></li>
                                                                <li><a href="#"><span>Shop Grid Sidebar</span></a></li>
                                                                <li><a href="#"><span>Shop List</span></a></li>
                                                                <li><a href="#"><span>Shop List Sidebar</span></a></li>
                                                                <li><a href="#"><span>Product Detail</span></a></li>
                                                                <li><a href="#"><span>Product Detail Sidebar</span></a></li>
                                                                <li><a href=""><span>Shopping Cart</span></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Contruction</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Checkout</span></a></li>
                                                                <li><a href="#"><span>Quick View</span></a></li>
                                                                <li><a href="#">Complete Order</a></li>
                                                                <li><a href="#">Account Information</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li><a href="#">Sport / Gift</a>
                                            <div class="wrap-popup">
                                                <div class="popup">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Sport</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Shop Grid</span></a></li>
                                                                <li><a href="#"><span>Shop Grid Sidebar</span></a></li>
                                                                <li><a href="#"><span>Shop List</span></a></li>
                                                                <li><a href="#"><span>Shop List Sidebar</span></a></li>
                                                                <li><a href="#"><span>Product Detail</span></a></li>
                                                                <li><a href="#"><span>Product Detail Sidebar</span></a></li>
                                                                <li><a href=""><span>Shopping Cart</span></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <h3><a href="#">Gift</a></h3>
                                                            <ul class="nav">
                                                                <li><a href="#"><span>Checkout</span></a></li>
                                                                <li><a href="#"><span>Quick View</span></a></li>
                                                                <li><a href="#">Complete Order</a></li>
                                                                <li><a href="#">Account Information</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        </li>
                                    </ul>
                                    <div class="side-banner"><img src="images/top-banner.jpg" alt="Flash Sale" class="img-responsive"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-sm-9 col-xs-12 jtv-rhs-header">
                    <div class="jtv-mob-toggle-wrap">
                        <div class="mm-toggle"><i class="fa fa-reorder"></i><span class="mm-label">Menu</span></div>
                    </div>
                    <div class="jtv-header-box">
                        <div class="search_cart_block">
                            <div class="search-box hidden-xs">
                                <form id="search_mini_form" action="#" method="get">
                                    <select name="cat" id="cat" class="cate-dropdown hidden-xs hidden-sm hidden-md">
                                        <option value="">Products</option>
                                        <option value="1">Buyings Request</option>
                                        <option value="2">Seller</option>
                                    </select>
                                    <input id="search" type="text" name="q" value="" class="searchbox" placeholder="Search entire store here..." maxlength="128">
                                    <button type="submit" title="Search" class="search-btn-bg" id="submit-button"><span class="hidden-sm">Search</span><i class="fa fa-search hidden-xs hidden-lg hidden-md"></i></button>
                                </form>
                            </div>
                            <div class="right_menu">
                                <div class="menu_top">
                                    <div class="top-cart-contain pull-right">
                                        <div class="mini-cart">
                                            <div class="basket"><a class="basket-icon" href="#"><i class="fa fa-shopping-basket"></i> Shopping Cart <span>3</span></a>
                                                <div class="top-cart-content">
                                                    <div class="block-subtitle">
                                                        <div class="top-subtotal">3 items, <span class="price">$399.49</span></div>
                                                    </div>
                                                    <ul class="mini-products-list" id="cart-sidebar">
                                                        <li class="item">
                                                            <div class="item-inner"><a class="product-image" title="product tilte is here" href="#"><img alt="product tilte is here" src="images/products/product-fashion-1.jpg"></a>
                                                                <div class="product-details">
                                                                    <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="fa fa-pencil"></i><span class="hidden">Edit item</span></a> </div>
                                                                    <p class="product-name"><a href="#">Product tilte is here</a></p>
                                                                    <strong>1</strong> x <span class="price">$119.99</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="item-inner"><a class="product-image" title="Product tilte is here" href="#"><img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"></a>
                                                                <div class="product-details">
                                                                    <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="fa fa-pencil"></i><span class="hidden">Edit item</span></a> </div>
                                                                    <p class="product-name"><a href="#">Product tilte is here</a></p>
                                                                    <strong>1</strong> x <span class="price">$79.66</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li class="item">
                                                            <div class="item-inner"><a class="product-image" title="Product tilte is here" href="#"><img alt="Product tilte is here" src="images/products/product-fashion-1.jpg"></a>
                                                                <div class="product-details">
                                                                    <div class="access"><a class="btn-remove1" title="Remove This Item" href="#">Remove</a> <a class="btn-edit" title="Edit item" href="#"><i class="fa fa-pencil"></i><span class="hidden">Edit item</span></a> </div>
                                                                    <p class="product-name"><a href="#">Product tilte is here</a></p>
                                                                    <strong>1</strong> x <span class="price">$99.89</span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <div class="actions"> <a href="shopping-cart.php" class="view-cart"><span>View Cart</span></a>
                                                        <button onclick="window.location.href='checkout.php'" class="btn-checkout" title="Checkout" type="button"><span>Checkout</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="language-box hidden-xs">
                                    <select class="selectpicker" data-width="fit">
                                        <option>English</option>
                                        <option>Vietnam</option>
                                    </select>
                                </div>
                                <div class="currency-box hidden-xs">
                                    <form class="form-inline">
                                        <div class="input-group">
                                            <div class="currency-addon">
                                                <select class="currency-selector">
                                                    <option data-symbol="$">USD</option>
                                                    <option data-symbol="₫">VND</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="top_section hidden-xs">
                            <div class="toplinks">
                                <div class="site-dir hidden-xs"> <a class="hidden-sm" href="#"><i class="fa fa-phone"></i> <strong>Hotline:</strong> +1 123 456 7890</a> <a href="mailto:support@example.com"><i class="fa fa-envelope"></i> support@example.com</a> </div>
                                <ul class="links">
                                    <li><a title="For Buyer" href="about.php">About</a></li>
                                    <li><a title="For Seller" href="contact.php">Contact</a></li>
                                    <li><a title="Register" href="register.php">Register</a></li>
                                    <li><a class="btnLogin" title="Login" href="login.php"><span>Login</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>