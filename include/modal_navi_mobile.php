<div id="loginModal" class="modal fade">
        <div class="modal-dialog newsletter-popup">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <h4 class="modal-title">LOGIN ACCOUNT</h4>
                    <form id="newsletter-form" method="post" action="#">
                        <div class="content-subscribe">
                            <div class="input-box">
                                <input type="text" class="input-text newsletter-subscribe" title="Sign up for our newsletter" name="email" placeholder="Enter your email address">
                            </div>
                            <div class="input-box">
                                <input type="password" class="input-text newsletter-subscribe" title="Sign up for our newsletter" name="password" placeholder="Enter your password">
                            </div>
                            <div class="actions">
                                <button class="button-subscribe" title="Subscribe" type="submit">Login</button>
                            </div>
                        </div>
                        <div class="subscribe-bottom">
                            <input name="notshowpopup" id="notshowpopup" type="checkbox">
                            Remember </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Newsletter Popup -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog newsletter-popup">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="modal-body">
                    <h4 class="modal-title">Subscribe</h4>
                    <form id="newsletter-form" method="post" action="#">
                        <div class="content-subscribe">
                            <div class="form-subscribe-header">
                                <label>For all the latest news, products, collection...</label>
                                <label>Subscribe now to get 20% off</label>
                            </div>
                            <div class="input-box">
                                <input type="text" class="input-text newsletter-subscribe" title="Sign up for our newsletter" name="email" placeholder="Enter your email address">
                            </div>
                            <div class="actions">
                                <button class="button-subscribe" title="Subscribe" type="submit">Subscribe</button>
                            </div>
                        </div>
                        <div class="subscribe-bottom">
                            <input name="notshowpopup" id="notshowpopup" type="checkbox">
                            Don’t show this popup again </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Mobile Menu -->
    <div id="jtv-mobile-menu">
        <ul>
            <li>
                <div class="mm-search">
                    <form id="mob-search" name="search">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
                                <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>
            <li><a href="index.html">Home</a>
            </li>
            <li><a href="#">Men's</a>
                <ul>
                    <li><a href="#">Jackets</a>
                        <ul>
                            <li><a href="#">Coats</a></li>
                            <li><a href="#">Outerwear</a></li>
                            <li><a href="#">Bags</a></li>
                            <li><a href="#">Dresses</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Watches</a>
                        <ul>
                            <li><a href="#">Fastrack</a></li>
                            <li><a href="#">Casio</a></li>
                            <li><a href="#">Sonata</a></li>
                            <li><a href="#">Maxima</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Footwear</a>
                        <ul>
                            <li><a href="#">Sports</a></li>
                            <li><a href="#">Flat Sandals</a></li>
                            <li><a href="#">Casual</a></li>
                            <li><a href="#">Sneakers</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Jwellery</a>
                        <ul>
                            <li><a href="#">Bracelets</a></li>
                            <li><a href="#">Necklaces &amp; Pendent</a></li>
                            <li><a href="#">Pendants</a></li>
                            <li><a href="#">Pins &amp; Brooches</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Suits</a>
                        <ul>
                            <li><a href="#">Casual Dresses</a></li>
                            <li><a href="#">Evening</a></li>
                            <li><a href="#">Designer</a></li>
                            <li><a href="#">Party</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Accessories</a>
                        <ul>
                            <li><a href="#">Trousers</a></li>
                            <li><a href="#">Jeans</a></li>
                            <li><a href="#">Clothing</a></li>
                            <li><a href="#">Shirts</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">Women's </a>
                <ul>
                    <li><a href="#">Clothing</a>
                        <ul>
                            <li><a href="#">Dress sale</a></li>
                            <li><a href="#">Sarees</a></li>
                            <li><a href="#">Kurta & kurti</a></li>
                            <li><a href="#">Dress materials</a></li>
                            <li><a href="#">Salwar kameez sets</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Jewellery</a>
                        <ul>
                            <li><a href="#">Rings</a></li>
                            <li><a href="#">Earrings</a></li>
                            <li><a href="#">Jewellery sets</a></li>
                            <li><a href="#">Pendants & lockets</a></li>
                            <li><a href="#">Plastic jewellery</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Beauty</a>
                        <ul>
                            <li><a href="#">Make up</a></li>
                            <li><a href="#">Hair care</a></li>
                            <li><a href="#">Deodorants</a></li>
                            <li><a href="#">Bath & body</a></li>
                            <li><a href="#">Skin care</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Watches</a>
                        <ul>
                            <li><a href="#">Fasttrack</a></li>
                            <li><a href="#">Casio</a></li>
                            <li><a href="#">Titan</a></li>
                            <li><a href="#">Tommy-Hilfiger</a></li>
                            <li><a href="#">Fossil</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Footwear</a>
                        <ul>
                            <li><a href="#">Flats</a></li>
                            <li><a href="#">Heels</a></li>
                            <li><a href="#">Boots</a></li>
                            <li><a href="#">Slippers</a></li>
                            <li><a href="#">Shoes</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Accesories</a>
                        <ul>
                            <li><a href="#">Backpacks</a></li>
                            <li><a href="#">Wallets</a></li>
                            <li><a href="#">Laptops Bags</a></li>
                            <li><a href="#">Belts</a></li>
                            <li><a href="#">Handbags</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="#">Kids</a>
                <ul>
                    <li><a href="#">Clothing</a>
                        <ul>
                            <li><a href="#">T-Shirts</a></li>
                            <li><a href="#">Shirts</a></li>
                            <li><a href="#">Trousers</a></li>
                            <li><a href="#">Sleep Wear</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Accesories</a>
                        <ul>
                            <li><a href="#">Backpacks</a></li>
                            <li><a href="#">Wallets</a></li>
                            <li><a href="#">Laptops Bags</a></li>
                            <li><a href="#">Belts</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Watches</a>
                        <ul>
                            <li><a href="#">Fastrack</a></li>
                            <li><a href="#">Casio</a></li>
                            <li><a href="#">Titan</a></li>
                            <li><a href="#">Maxima</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Footwear</a>
                        <ul>
                            <li><a href="#">Casual</a></li>
                            <li><a href="#">Sports</a></li>
                            <li><a href="#">Formal</a></li>
                            <li><a href="#">Sandals</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Computer</a>
                        <ul>
                            <li><a href="#">External Hard Disk</a></li>
                            <li><a href="#">Pendrives</a></li>
                            <li><a href="#">Headphones</a></li>
                            <li><a href="#">PC Components</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Appliances</a>
                        <ul>
                            <li><a href="#">Vaccum Cleaners</a></li>
                            <li><a href="#">Indoor Lighting</a></li>
                            <li><a href="#">Kitchen Tools</a></li>
                            <li><a href="#">Water Purifier</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="top-links">
            <ul class="links">
                <li><a title="For Buyer" href="#">For Buyer</a></li>
                <li><a title="For Seller" href="#">For Seller</a></li>
                <li><a title="Register" href="register.php">Register</a></li>
                <li class="last"><a title="Login" href="login.php"><span>Login</span></a></li>
            </ul>
            <div class="language-box">
                <select class="selectpicker" data-width="fit">
                    <option>English</option>
                    <option>VietNam</option>
                </select>
            </div>
            <div class="currency-box">
                <form class="form-inline">
                    <div class="input-group">
                        <div class="currency-addon">
                            <select class="currency-selector">
                                <option data-symbol="$">USD</option>
                                <option data-symbol="₫">VND</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>