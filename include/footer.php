<footer>
    <div class="footer-inner">
        <div class="news-letter">
            <div class="container">
                <div class="heading text-center">
                    <h2>Just Subscribe Now!</h2>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec faucibus maximus vehicula. Sed feugiat, tellus vel tristique posuere.</span>
                </div>
                <form>
                    <input type="email" placeholder="Enter your email address" required>
                    <button type="submit">Send me</button>
                </form>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <h4>About</h4>
                    <div class="contacts-info">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                        <address>
                            <i class="fa fa-location-arrow"></i> <span>Company, 12/34 - West 21st Street,<br>
                                New York, USA</span>
                        </address>
                        <div class="phone-footer"><i class="fa fa-phone"></i> +1 123 456 98765</div>
                        <div class="email-footer"><i class="fa fa-envelope"></i> <a href="mailto:support@example.com">support@example.com</a> </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <h4>Helpful Links</h4>
                    <ul class="links">
                        <li><a href="#">Products</a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-2 col-sm-6 col-xs-12">
                    <h4>Shop</h4>
                    <ul class="links">
                        <li><a href="about.php">About Us</a></li>
                        <li><a href="contact.php">Contact</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-lg-3 col-md-4 col-sm-6">
                    <div class="social">
                        <h4>Follow Us</h4>
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-skype"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-xs-12 coppyright text-center">© 2019 Fabulous, All rights reserved.</div>
            </div>
        </div>
    </div>
</footer>