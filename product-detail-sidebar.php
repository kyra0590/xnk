﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="product-page">
    <!-- Mobile Menu -->
    <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
         <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- Breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>
                            <li class="home"> <a href="index.html" title="Go to Home Page">Home</a> <span>/</span></li>
                            <li><a href="shop-grid-sidebar.html" title="">Women</a> <span>/ </span></li>
                            <li><a href="shop-grid-sidebar.html" title="">Clothing</a> <span>/</span></li>
                            <li> <strong>Product tilte is here </strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumbs End -->
        <!-- Main Container -->
        <section class="main-container col2-left-layout">
            <div class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9 col-xs-12">
                            <div class="product-view">
                                <div class="product-essential">
                                    <form action="#" method="post" id="product_addtocart_form">
                                        <div class="product-img-box col-lg-5 col-sm-5 col-xs-12">
                                            <div class="product-image">
                                                <div class="product-full">
                                                    <div class="new-label new-top-left"> New </div>
                                                    <img id="product-zoom" src="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg" alt="product-image" />
                                                </div>
                                                <div class="more-views">
                                                    <div class="slider-items-products">
                                                        <div id="jtv-more-views-img" class="product-flexslider hidden-buttons product-img-thumb">
                                                            <div class="slider-items slider-width-col4 block-content">
                                                                <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom" src="images/products/product-fashion-1.jpg" alt="product-image" /> </a></div>
                                                                <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom" src="images/products/product-fashion-1.jpg" alt="product-image" /> </a></div>
                                                                <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom" src="images/products/product-fashion-1.jpg" alt="product-image" /> </a></div>
                                                                <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom" src="images/products/product-fashion-1.jpg" alt="product-image" /> </a> </div>
                                                                <div class="more-views-items"> <a href="#" data-image="images/products/product-fashion-1.jpg" data-zoom-image="images/products/product-fashion-1.jpg"> <img id="product-zoom" src="images/products/product-fashion-1.jpg" alt="product-image" /> </a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end: more-images -->
                                        </div>
                                        <div class="product-shop col-lg-7 col-sm-7 col-xs-12">
                                            <div class="product-name">
                                                <h1>Product tilte is here</h1>
                                            </div>
                                            <div class="price-block">
                                                <div class="price-box">
                                                    <p class="special-price"> <span class="price-label">Special Price</span><span class="price"> Liên hệ </span></p>
                                                    <p class="availability in-stock"><span>In Stock</span></p>
                                                </div>
                                            </div>
                                            <div class="short-description">
                                                <h2>Quick Overview</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero.</p>
                                            </div>
                                            <div class="add-to-box">
                                                <div class="add-to-cart">
                                                    <div class="pull-left">
                                                        <div class="custom pull-left">
                                                            <label>Quantity:</label>
                                                            <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="fa fa-minus">&nbsp;</i></button>
                                                            <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                                                            <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="fa fa-plus">&nbsp;</i></button>
                                                        </div>
                                                    </div>
                                                    <button onClick="productAddToCartForm.submit(this)" class="button btn-cart" title="Add to Cart" type="button"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                                </div>
                                            </div>
                                            <div class="email-addto-box">
                                            <ul class="add-to-links">
                                              <li><a class="link-wishlist" href="#"><i class="fa fa-product-hunt"></i><span> Name Nhà sản xuất</span></a></li>
                                            </ul>
                                          </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="product-collateral">
                                <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                                    <li class="active"> <a href="#product_tabs_description" data-toggle="tab"> Product Description </a></li>
                                    <li><a href="#product_tabs_custom" data-toggle="tab">Note</a></li>
                                </ul>
                                <div id="productTabContent" class="tab-content">
                                    <div class="tab-pane fade in active" id="product_tabs_description">
                                        <div class="std">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla. Donec a neque libero. Pellentesque aliquet, sem eget laoreet ultrices, ipsum metus feugiat sem, quis fermentum turpis eros eget velit. Donec ac tempus ante. Fusce ultricies massa massa. Fusce aliquam, purus eget sagittis vulputate, sapien libero hendrerit est, sed commodo augue nisi non neque. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempor, lorem et placerat vestibulum, metus nisi posuere nisl, in accumsan elit odio quis mi. Cras neque metus, consequat et blandit et, luctus a nunc. Etiam gravida vehicula tellus, in imperdiet ligula euismod eget. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nam erat mi, rutrum at sollicitudin rhoncus, ultricies posuere erat. Duis convallis, arcu nec aliquam consequat, purus felis vehicula felis, a dapibus enim lorem nec augue.</p>
                                            <p> Nunc facilisis sagittis ullamcorper. Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus. Sed et lorem nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean eleifend laoreet congue. Vivamus adipiscing nisl ut dolor dignissim semper. Nulla luctus malesuada tincidunt. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer enim purus, posuere at ultricies eu, placerat a felis. Suspendisse aliquet urna pretium eros convallis interdum. Quisque in arcu id dui vulputate mollis eget non arcu. Aenean et nulla purus. Mauris vel tellus non nunc mattis lobortis.</p>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="product_tabs_custom">
                                        <div class="product-tabs-content-inner clearfix">
                                            <p>Lorem Ipsum is
                                                simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                                has been the industry's standard dummy text ever since the 1500s, when
                                                an unknown printer took a galley of type and scrambled it to make a type
                                                specimen book. It has survived not only five centuries, but also the
                                                leap into electronic typesetting, remaining essentially unchanged. It
                                                was popularised in the 1960s with the release of Letraset sheets
                                                containing Lorem Ipsum passages, and more recently with desktop
                                                publishing software like Aldus PageMaker including versions of Lorem
                                                Ipsum.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar col-sm-3 col-xs-12">
                            <aside class="sidebar">
                                <div class="custom-slider">
                                    <div>
                                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                            <ol class="carousel-indicators">
                                                <li class="active" data-target="#carousel-example-generic" data-slide-to="0"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                            </ol>
                                            <div class="carousel-inner">
                                                <div class="item active"><img src="images/slide3.jpg" alt="New Arrivals">
                                                    <div class="carousel-caption">
                                                        <h3><a title=" Sample Product" href="#">New Arrivals</a></h3>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                    </div>
                                                </div>
                                                <div class="item"><img src="images/slide1.jpg" alt="Top Fashion">
                                                    <div class="carousel-caption">
                                                        <h3><a title=" Sample Product" href="#">Top Fashion</a></h3>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                    </div>
                                                </div>
                                                <div class="item"><img src="images/slide2.jpg" alt="Mid Season">
                                                    <div class="carousel-caption">
                                                        <h3><a title=" Sample Product" href="#">Mid Season</a></h3>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <a class="left carousel-control" href="#" data-slide="prev"> <span class="sr-only">Previous</span></a> <a class="right carousel-control" href="#" data-slide="next"> <span class="sr-only">Next</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="block block-cart">
                                    <div class="block-title ">
                                        <h3>My Cart</h3>
                                    </div>
                                    <div class="block-content">
                                        <div class="summary">
                                            <p class="amount">There are <a href="shopping_cart.html">3 items</a> in your cart.</p>
                                        </div>
                                        <div class="ajax-checkout">
                                            <button class="button button-checkout" title="Submit" type="submit"><span>Checkout</span></button>
                                        </div>
                                        <p class="block-subtitle">Recently added item(s) </p>
                                        <ul>
                                            <li class="item"> <a href="shopping_cart.html" title="Product Title Here" class="product-image"><img src="images/products/product-fashion-1.jpg" alt="Product Title Here"></a>
                                                <div class="product-details">
                                                    <div class="access"> <a href="shopping_cart.html" title="Remove This Item" class="jtv-btn-remove"> <span class="icon"></span> Remove </a> </div>
                                                    <p class="product-name"> <a href="shopping_cart.html">Product Title Here</a> </p>
                                                    <strong>1</strong>
                                                </div>
                                            </li>
                                            <li class="item"> <a href="shopping_cart.html" title="Product Title Here" class="product-image"><img src="images/products/product-fashion-1.jpg" alt="Product Title Here"></a>
                                                <div class="product-details">
                                                    <div class="access"> <a href="shopping_cart.html" title="Remove This Item" class="jtv-btn-remove"> <span class="icon"></span> Remove </a> </div>
                                                    <p class="product-name"> <a href="shopping_cart.html">Product Title Here</a> </p>
                                                    <strong>1</strong>
                                                </div>
                                            </li>
                                            <li class="item"> <a href="shopping_cart.html" title="Product Title Here" class="product-image"><img src="images/products/product-fashion-1.jpg" alt="Product Title Here"></a>
                                                <div class="product-details">
                                                    <div class="access"> <a href="shopping_cart.html" title="Remove This Item" class="jtv-btn-remove"> <span class="icon"></span> Remove </a> </div>
                                                    <p class="product-name"> <a href="shopping_cart.html">Product Title Here</a> </p>
                                                    <strong>1</strong>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Main Container End -->
        <!-- Footer -->
        <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
    <script src="js/cloud-zoom.js"></script>
</body>

</html>