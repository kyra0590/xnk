﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="account-information-page">
    <!-- Mobile Menu -->
    <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
         <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- main-container -->
        <div class="main-container col2-right-layout">
            <div class="main container">
                <div class="row">
                    <section class="col-md-9 col-xs-12">
                        <div class="col-main">
                            <div class="my-account">
                                <div class="page-title">
                                    <h2>Register Account</h2>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-xs-12">
                                        <div class="title-box">
                                            <h3>Account Information</h3>
                                        </div>
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="form-group">
                                                    <label for="fname">First Name <span class="required">*</span></label>
                                                    <input type="text" name="fname" id="fname" class="form-control" placeholder="Mr.">
                                                </div>
                                                <div class="form-group">
                                                    <label for="lname">Last Name <span class="required">*</span></label>
                                                    <input type="text" name="lname" id="lname" class="form-control" placeholder="Roy">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <label for="emailAddress">Email Address <span class="required">*</span></label>
                                                    <input type="email" name="email" id="emailAddress" class="form-control" placeholder="vince-roy@example.com">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-xs-12">
                                        <div class="title-box">
                                            <h3>Change Password</h3>
                                        </div>
                                        <ul class="list-unstyled">
                                            <li>
                                                <div class="form-group">
                                                    <label for="cpassword">Current Password <span class="required">*</span></label>
                                                    <input type="password" name="cpassword" id="cpassword" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                    <label for="npassword">New Password <span class="required">*</span></label>
                                                    <input type="password" name="npassword" id="npassword" class="form-control">
                                                </div>
                                            </li>
                                            <li>
                                                <div class="form-group">
                                                    <label for="cnewpassword">Confirm New Password <span class="required">*</span></label>
                                                    <input type="password" name="cnewpassword" id="cnewpassword" class="form-control">
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="buttons-set">
                                    <button id="send2" name="send" type="submit" class="button login"><span>Save</span></button>
                                    <span class="required pull-right"><b>*</b> Required Field</span> </div>
                            </div>
                        </div>
                    </section>
                    <aside class="col-right sidebar col-md-3 col-xs-12">
                        <div class="block block-compare">
                               <img width="100%" alt="SOOK" src="https://cdn.thaitrade.com/media/magestore/bannerslider/images/0/0/002_13.jpg">
                        </div>
                    </aside>
                    <aside class="col-right sidebar col-md-3 col-xs-12">
                        <div class="block block-compare">
                               <img width="100%" alt="SOOK" src="https://cdn.thaitrade.com/media/magestore/bannerslider/images/0/0/001_11.jpg">
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <!--End main-container -->
        <!-- Footer -->
       <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/revslider.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
</body>

</html>