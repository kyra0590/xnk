﻿<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
    <meta name="description" content="Fabulous is a creative, clean, fully responsive, powerful and multipurpose HTML Template with latest website trends. Perfect to all type of fashion stores.">
    <meta name="keywords" content="HTML,CSS,womens clothes,fashion,mens fashion,fashion show,fashion week">
    <meta name="author" content="JTV">
    <title>Fabulous - Multipurpose Online Marketplace HTML Template</title>
    <!-- Favicons Icon -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="css/styles.css" media="all">
</head>

<body class="contact-us-page">
    <!-- Mobile Menu -->
     <?php include('include/modal_navi_mobile.php') ?>
    <div id="page">
        <!-- Header -->
         <?php include('include/header.php') ?>
        <!-- end header -->
        <!-- breadcrumbs -->
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ul>
                            <li class="home"> <a title="Go to Home Page" href="index.html">Home</a> <span>/</span></li>
                            <li> <strong>Contact Us</strong> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- main-container -->
        <div class="main-container col2-right-layout">
            <div class="container">
                <div class="row">
                    <section class="col-sm-9">
                        <div class="col-main">
                            <div class="static-inner">
                                <div class="page-title">
                                    <h2>Contact Us</h2>
                                </div>
                                <div class="static-contain">
                                    <fieldset class="group-select">
                                        <ul>
                                            <li id="billing-new-address-form">
                                                <fieldset>
                                                    <input type="hidden" name="billing[address_id]" value="" id="billing:address_id">
                                                    <ul>
                                                        <li>
                                                            <div class="customer-name">
                                                                <div class="input-box name-firstname">
                                                                    <label for="billing:firstname"> First Name<span class="required">*</span></label>
                                                                    <br>
                                                                    <input type="text" id="billing:firstname" name="billing[firstname]" value="" title="First Name" class="input-text ">
                                                                </div>
                                                                <div class="input-box name-lastname">
                                                                    <label for="billing:lastname"> Email Address <span class="required">*</span></label>
                                                                    <br>
                                                                    <input type="text" id="billing:lastname" name="billing[lastname]" value="" title="Last Name" class="input-text">
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="input-box">
                                                                <label for="billing:company">Company</label>
                                                                <br>
                                                                <input type="text" id="billing:company" name="billing[company]" value="" title="Company" class="input-text">
                                                            </div>
                                                            <div class="input-box">
                                                                <label for="billing:email">Telephone <span class="required">*</span></label>
                                                                <br>
                                                                <input type="text" name="billing[email]" id="billing:email" value="" title="Email Address" class="input-text validate-email">
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <label>Address <span class="required">*</span></label>
                                                            <br>
                                                            <input type="text" title="Street Address" name="billing[street][]" value="" class="input-text required-entry">
                                                        </li>
                                                        <li>
                                                            <input type="text" title="Street Address 2" name="billing[street][]" value="" class="input-text required-entry">
                                                        </li>
                                                        <li class="">
                                                            <label for="comment">Comment<em class="required">*</em></label>
                                                            <br>
                                                            <div style="float:none" class="">
                                                                <textarea name="comment" id="comment" title="Comment" class="required-entry input-text" cols="5" rows="3"></textarea>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </fieldset>
                                            </li>
                                            <li>
                                                <p class="require"><em class="required">* </em>Required Fields</p>
                                                <div class="buttons-set">
                                                    <button type="submit" title="Submit" class="button submit"> <span> Submit </span></button>
                                                </div>
                                            </li>
                                        </ul>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </section>
                    <aside class="col-right sidebar col-sm-3 wow">
                        <div class="block block-company">
                            <div class="block-title">Company </div>
                            <div class="block-content">
                                <ol id="recently-viewed-items">
                                    <li class="item odd"><a href="#"><i class="fa fa-angle-right"></i> About Us</a></li>
                                    <li class="item even"><a href="#"><i class="fa fa-angle-right"></i> Sitemap</a></li>
                                    <li class="item  odd"><a href="#"><i class="fa fa-angle-right"></i> Terms of Service</a></li>
                                    <li class="item even"><a href="#"><i class="fa fa-angle-right"></i> Search Terms</a></li>
                                    <li class="item last"><strong><i class="fa fa-angle-right"></i> Contact Us</strong></li>
                                </ol>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <!--End main-container -->
        <!-- Footer -->
         <?php include('include/footer.php') ?>
    </div>
    <!-- JavaScript -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/revslider.js"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/mob-menu.js"></script>
</body>

</html>